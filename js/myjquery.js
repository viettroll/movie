 $(function() {
    $('.tab_content').hide();
    $('.tab_content:first').show();
    $('.tabs li:first').addClass('active');
    $('.tabs li').click(function(event) {
        event.preventDefault();
        $('.tabs li').removeClass('active');
        $(this).addClass('active');
        $('.tab_content').hide();

        var selectTab = $(this).find('a').attr("href");

        $(selectTab).fadeIn();
    });
});

  $('.slide-adv').owlCarousel({
    loop: true,
    nav: true,
    smartSpeed: 2000,
    dots:false,
    autoplay: true,
    autoplayTimeout: 5000,
    // navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});

$(".ul-item").slimScroll({
    size: '8px', 
    width: '100%', 
    height: '100%', 
    color: '#ff4800', 
    allowPageScroll: true, 
    alwaysVisible: true     
  });

  
$(".list-trailer").slimScroll({
    size: '8px', 
    width: '100%', 
    height: '100%', 
    color: '#ff4800', 
    allowPageScroll: true, 
    alwaysVisible: true     
  });

$(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
        $('.scrollup').fadeIn();
    } else {
        $('.scrollup').fadeOut();
    }
});

$('.scrollup').click(function() {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});

$('.cat-film-peo').owlCarousel({
    loop:true,
    nav:true,
    smartSpeed: 2000,
    dots:false,
    // autoplay: true,
    // autoplayTimeout: 5000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
